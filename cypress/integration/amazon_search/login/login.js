import {Given, When, Then} from "cypress-cucumber-preprocessor/steps"
import MainPage from "../../pages/mainPage";
import FindingBooksPage from "../../pages/findingBooksPage";

const mainPage = new MainPage()
const findingBooksPage = new FindingBooksPage()

Given('I am in the amazon mane page',()=>{

    cy.visit("https://www.amazon.com/");
    cy.wait(10000)

})

When('I enter needed data to find books',()=>{
    mainPage.search()
    mainPage.clickSearchButton()
})

Then('I should be able to have list of book and check that book is exist',()=>{

    findingBooksPage.assertBooks()

})