cy.functions = {
    putBestSellerAndAuthor(length, bestSellerID) {
        for (let i = 1; i < length; i++) {
            cy.get('[data-index="' + i + '"]').invoke('attr', 'data-asin').then(id => {
                cy.functions.setBestSeller(bestSellerID, id)
            })
        }
    },

    checkBestSeller(length){
        cy.get(cy.constants.body).then(bestSeller => {
            if(bestSeller.find(cy.constants.bestSeller).length > 0){
                cy.functions.assertBestSeller(length)
            }
        })
    },

    assertBestSeller(length){
        cy.get(cy.constants.bestSeller).then(id => {
            for (let i=0;i<id.length; i++) {
                let id1 = cy.utils.replaceChAfter(id.eq(i).attr('id'))
                cy.functions.putBestSellerAndAuthor(length, id1)
            }
        })
    },

    setBestSeller(bestSellerID, bookID){
        if (bookID === bestSellerID) {
            expect(bookID).to.have.equal(bestSellerID)
        }
    },

    checkBookList(length, booksName){
        cy.get(cy.constants.price1).find(cy.constants.price2).find(cy.constants.price3).then(price => {
                cy.get(cy.constants.author1).find(cy.constants.author2).find(cy.constants.author3).find(cy.constants.author3).find(cy.constants.author4).find(cy.constants.author5).then(author => {
                        cy.functions.assertBooks(length, booksName, price, author)
                    }
                )
            }
        )
    },

    assertPriceBook(length, booksName, bookName, searchedBookPrice) {
        cy.get(cy.constants.usedPrice).then(priceBook => {
                for (let i = 0; i < length; i++) {
                    cy.functions.assertBook(booksName.eq(i).text(), bookName, searchedBookPrice.eq(i).text(), priceBook.text())
                }
            }
        )
    },

    assertAuthorBook(length, booksName, bookName, searchedBookAuthor) {
        cy.get(cy.constants.authorBook).then(authorBook => {
                let j = 0;
                const ch2 = cy.utils.insert(authorBook.text(), ' and ', 12);
                for (let i = 0; i < length; i++) {
                    const ch1 = cy.utils.replaceCh(searchedBookAuthor.eq(j).text());
                    cy.functions.assertBook(booksName.eq(i).text(), bookName, ch1, ch2)
                    j = j + 3;
                }
            }
        )
    },

    assertBooks(length, booksName, booksPrice, booksAuthor) {
        cy.visit(cy.constants.findBookURL)
        cy.get(cy.constants.productTitle).then(name => {
            const nameBook = name.text().slice(1, 29)
            for (let i = 0; i < length; i++) {
                if (booksName.eq(i).text() === nameBook) {
                    expect(booksName.eq(i).text()).to.have.equal(nameBook)
                }
            }
            cy.functions.assertPriceBook(length, booksName, nameBook, booksPrice)
            cy.functions.assertAuthorBook(length, booksName, nameBook, booksAuthor)

        })
    },

    assertBook(booksName, bookName, booksParam, bookParam) {
        if (booksName === bookName) {
            if (booksParam === bookParam) {
                expect(booksParam).to.have.equal(bookParam)
            } else {
                expect(booksParam).to.not.equal(bookParam)
            }
        }
    }
}