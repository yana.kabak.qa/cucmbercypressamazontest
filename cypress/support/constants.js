cy.constants = {
    mainPage:'https://www.amazon.com/',
    searchArea:'#twotabsearchtextbox',
    keyWord:'Java',
    searchCategoriesButton:'#searchDropdownBox',
    selectCategories:'Books',
    searchButton:'#nav-search-submit-button',
    nameBookList:'span.a-size-medium.a-color-base.a-text-normal',
    price1:'div.a-section.a-spacing-none.a-spacing-top-small',
    price2:'span[data-a-size=l]',
    price3:'span.a-offscreen',
    author1:'div[data-component-type=s-search-result]',
    author2:'div.sg-col-inner',
    author3:'div.a-section.a-spacing-none',
    author4:'div.a-row.a-size-base.a-color-secondary',
    author5:'div.a-row',
    findBookURL:'https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_2?dchild=1&keywords=Java&qid=1610356790&s=books&sr=1-2',
    productTitle:'[id=productTitle]',
    usedPrice:'[id=usedPrice]',
    authorBook:'a.a-link-normal.contributorNameID',
    bestSeller:'span.a-badge',
    body:'body'
}